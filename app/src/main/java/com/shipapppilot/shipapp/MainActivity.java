package com.shipapppilot.shipapp;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.shipapppilot.shipapp.service.DeilWindow;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MainActivity extends AppCompatActivity {

    String token;
    private DatabaseReference mDatabase;
    ListView myListView;
    ImageButton gdyniaButton;
    ImageButton gdanskButton;
    ImageButton favoriteButton;
    static String portName;
    List<String> ports = new ArrayList<>();
    Ship shipUlubiony;
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        token = FirebaseInstanceId.getInstance().getToken();
        ports.add("Gdansk");
        ports.add("Gdynia");

        Log.d("ACTIVITY", "OnCreate");
        Intent readedIntent = getIntent();
        String passingBy = readedIntent.getStringExtra("passingBy");
        if (passingBy != null) {
            if (passingBy.equals("favorite")) {
                favoritesShips();
            }
            if (passingBy.equals("value")) {
                portName = readedIntent.getStringExtra("portZintenta");
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                mDatabase = database.getReference();
                DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(portName);
                valueListenerLoop();
            }

        }else favoritesShips();
        Log.d("OnCreate", "On CREATE!");


        setContentView(R.layout.activity_main);
        ActionBar actionBar = getSupportActionBar();

   //     actionBar.setHomeButtonEnabled(true);
        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#0099FF"));
        actionBar.setBackgroundDrawable(colorDrawable);
        myListView = findViewById(R.id.listViewId);
        gdyniaButton = findViewById(R.id.gdyniaButton);
        gdyniaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               FirebaseDatabase database = FirebaseDatabase.getInstance();
                mDatabase = database.getReference();
               portName = "Gdynia";
              DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child(portName);
                valueListenerLoop();

            }
        });
        gdanskButton = findViewById(R.id.gdanskButton);
        gdanskButton.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                mDatabase = database.getReference();
                portName = "Gdansk";
                DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(portName);
                valueListenerLoop();

            }
        }));

        favoriteButton = findViewById(R.id.favoriteButton);
        favoriteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                favoritesShips();

            }
        });
        Log.d("ACTIVITY", "OnStart");

    }
    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            finishAffinity();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Naciśnij jeszcze raz by wyjść z aplikacji", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;

            }
        }, 2000);
    }



    @Override
    protected void onStart() {
        super.onStart();
        Log.d("ACTIVITY", "OnResume");
    }


    public void favoritesShips() {
        final List<Ship> allShipsList = new ArrayList<>();
        final List<HashMap<String, String>> finalAllShipsListView = new ArrayList<>();
        finalAllShipsListView.clear();
        allShipsList.clear();

        for (final String actualPortIterator : ports) {
            Log.d("iteruję po Ports ", actualPortIterator);
            FirebaseDatabase database = FirebaseDatabase.getInstance();
        mDatabase = database.getReference();
        mDatabase.child(actualPortIterator).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot fauvoriteShip : dataSnapshot.getChildren()) {

                    Object tokens = fauvoriteShip.child("tokens").getValue();
                    try {
                        if (((HashMap) tokens).values().contains(token)) {
                             shipUlubiony = fauvoriteShip.getValue(Ship.class);
                            final String key = fauvoriteShip.getKey();
                            shipUlubiony.setId(key);
                            shipUlubiony.setPortNameChoosen(actualPortIterator);
                            /*finalAllShipsListView.add(shipUlubiony.getName() + " " + shipUlubiony.getArrivalPlace());*/
                            HashMap<String, String> item = new HashMap<String, String>();
                            item.put("name", shipUlubiony.getName());
                            item.put("purpose", shipUlubiony.getDate());
                            item.put("berth" , shipUlubiony.getArrivalPlace());
                            finalAllShipsListView.add(item);
                            allShipsList.add(shipUlubiony);
                          //  Log.d("Loogi", String.valueOf(fauvoriteShip.child("name")));

                        }
                    } catch (Exception e) {
                       // System.out.println(e);
                    }



                    ListView listView = findViewById(R.id.listViewId);
                    Intent intent = getIntent();

                    finalAllShipsListView.remove(intent.getStringExtra("DeilShipId"));
                    for (Ship shipSzukany : allShipsList){
                        if (shipSzukany.getId().equals(intent.getStringExtra("DeilShipId")))
                        {allShipsList.remove(shipSzukany);
                    }

                    }
                 //   Log.d("FirebaseList", String.valueOf(allShipsList));
                 //   final ArrayAdapter adapter = new ArrayAdapter(MainActivity.this, android.R.layout.simple_list_item_1, finalAllShipsListView);
                    String[] from = { "name", "purpose", "berth" };
                    int[] to = { R.id.textView1, R.id.textView2, R.id.textView3 };
                    SimpleAdapter adapter = new SimpleAdapter(MainActivity.this, finalAllShipsListView, R.layout.oneline, from,to );
                    listView.setAdapter(adapter);

                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Ship shipUpdated = allShipsList.get(position);
                            Intent myIntent = new Intent(MainActivity.this, DeilWindow.class);
                            String idOfParticularShip = shipUpdated.getId();
                            String nameOfParticularShip = shipUpdated.getName();
                            String dateOfParticularShip = shipUpdated.getDate();
                            String timeOfParticularShip = shipUpdated.getTime();
                            String infoOfParticularShip = shipUpdated.getInfo();
                            String arrivalOfParticularShip = shipUpdated.getArrivalPlace();
                            String departureOfParticularShip = shipUpdated.getDeparturePlace();
                            String portNameChoosen = shipUpdated.getPortNameChoosen();
                            myIntent.putExtra("shipName", nameOfParticularShip);
                            myIntent.putExtra("shipDate", dateOfParticularShip);
                            myIntent.putExtra("shipTime", timeOfParticularShip);
                            myIntent.putExtra("shipInfo", infoOfParticularShip);
                            myIntent.putExtra("arrivalPlace", arrivalOfParticularShip);
                            myIntent.putExtra("departurePlace", departureOfParticularShip);
                            myIntent.putExtra("shipId", idOfParticularShip);
                            myIntent.putExtra("tokenId", token);
                            myIntent.putExtra("portName",portNameChoosen);
                            myIntent.putExtra("imFrom","favorite");
                            view.getContext().startActivity(myIntent);
                        }
                    });
                    adapter.notifyDataSetChanged();


                    /*Ship value = fauvoriteShip.getValue(Ship.class);
                    Map<String, String> tokens = value.getToken();
                    boolean contains = tokens.values().contains(token);
                    Log.d("czyTokenToMojToken", String.valueOf(contains));
                    Log.d("tokeny", String.valueOf(tokens));*/


                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }

        });
    }

}




    Ship ship;
    List<Ship> listaStatkow;
    ArrayList<Map<String, String>> lista;


    public void valueListenerLoop (){
        listaStatkow = new ArrayList<>();
        lista = new ArrayList<>();

        mDatabase.child(portName).addListenerForSingleValueEvent(new ValueEventListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                listaStatkow.clear();
                lista.clear();
                ship = null;
                for (DataSnapshot eachShip : dataSnapshot.getChildren()) {
                    final String key = eachShip.getKey();
                    //   Log.d("FirebaseKey", key);
                    final Ship ship = eachShip.getValue(Ship.class);

                    //===========
                    Object tokens = eachShip.child("tokens").getValue();
                    try {
                        if (((HashMap) tokens).values().contains(token)) {
                            if (ship != null) {
                                ship.setObserved(true);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    //============
                    String shipName = ship.getName();
                    ship.setId(key);
                    ship.getDate();
                    ship.getTime();
                    listaStatkow.add(ship);
                    HashMap<String, String> item = new HashMap<String, String>();
                    item.put("name", ship.getName());
                    item.put("purpose", ship.getDate());
                    item.put("berth" , ship.getArrivalPlace());
                    lista.add(item);

                }
                if (portName.equals("Gdansk")) {
                    Collections.sort(lista, new Comparator<Map<String, String>>() {
                        @Override
                        public int compare(Map<String, String> o1, Map<String, String> o2) {
                            return o1.get("name").compareTo(o2.get("name"));
                        }
                    });

                    listaStatkow.sort(Comparator.comparing(Ship::getName));
                }


                Log.d("stop", "stop tutaj");
                ListView listView = findViewById(R.id.listViewId);
                Log.d("FirebaseList", String.valueOf(listaStatkow));

                String[] from = { "name", "purpose", "berth" };
                int[] to = { R.id.textView1, R.id.textView2, R.id.textView3 };
                SimpleAdapter adapter = new SimpleAdapter(MainActivity.this, lista, R.layout.oneline, from,to );
               // final ArrayAdapter adapter = new ArrayAdapter(MainActivity.this, R.layout.oneline, R.id.textView2, lista);
                listView.setAdapter(adapter);


                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Ship shipUpdated = listaStatkow.get(position);
                        Intent myIntent = new Intent(MainActivity.this, DeilWindow.class);
                        String idOfParticularShip = shipUpdated.getId();
                        String nameOfParticularShip = shipUpdated.getName();
                        String dateOfParticularShip = shipUpdated.getDate();
                        String timeOfParticularShip = shipUpdated.getTime();
                        String infoOfParticularShip = shipUpdated.getInfo();
                        String arrivalOfParticularShip = shipUpdated.getArrivalPlace();
                        String departureOfParticularShip = shipUpdated.getDeparturePlace();
                        Map<String, String> token = shipUpdated.getToken();
                        boolean isObserved = shipUpdated.isObserved();
                        myIntent.putExtra("shipName", nameOfParticularShip);
                        myIntent.putExtra("shipDate", dateOfParticularShip);
                        myIntent.putExtra("shipTime", timeOfParticularShip);
                        myIntent.putExtra("shipInfo", infoOfParticularShip);
                        myIntent.putExtra("arrivalPlace", arrivalOfParticularShip);
                        myIntent.putExtra("departurePlace", departureOfParticularShip);
                        myIntent.putExtra("shipId", idOfParticularShip);
                        myIntent.putExtra("tokenId", MainActivity.this.token);
                        myIntent.putExtra("portName", portName);
                        myIntent.putExtra("imFrom", "value");
                        myIntent.putExtra("isObserved", isObserved);
                        myIntent.putExtra("mIobserved", isObserved);
                        view.getContext().startActivity(myIntent);
                    }
                });
                adapter.notifyDataSetChanged();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

}