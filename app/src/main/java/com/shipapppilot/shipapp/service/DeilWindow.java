package com.shipapppilot.shipapp.service;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.shipapppilot.shipapp.MainActivity;
import com.shipapppilot.shipapp.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;


public class DeilWindow extends AppCompatActivity {

    String shipId;
    String shipName;
    String shipDate;
    String shipTime;
    String shipInfo;
    String arrivalPlace;
    String departurePlace;
    String token;
    String portName;
    String imFrom;
    boolean mIobserved;


    Context mContext;
    TextView textViewShipName;
    TextView textViewShipDate;
    TextView textViewShipTime;
    TextView textViewShipInfo;
    TextView textViewShipArrival;
    TextView textViewShipDeparture;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detailwindow);
        ActionBar actionBar = getSupportActionBar();
        actionBar.getTitle();
        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#0099FF"));
        actionBar.setBackgroundDrawable(colorDrawable);
        actionBar.setDisplayHomeAsUpEnabled(true);


        textViewShipName = findViewById(R.id.textViewIdShipName);
        textViewShipDate = findViewById(R.id.textViewIdShipDate);
        textViewShipTime = findViewById(R.id.textViewIdShipTime);
        textViewShipInfo = findViewById(R.id.textViewIdShipInfo);
        textViewShipArrival = findViewById(R.id.textViewIdShipArrival);
        textViewShipDeparture = findViewById(R.id.textViewIdShipDeparture);


        mContext = getApplicationContext();

        Intent intent = getIntent();
        shipName = intent.getStringExtra("shipName");
        shipDate = intent.getStringExtra("shipDate");
        shipTime = intent.getStringExtra("shipTime");
        shipInfo = intent.getStringExtra("shipInfo");
        portName = intent.getStringExtra("portName");
        imFrom = intent.getStringExtra("imFrom");
        arrivalPlace = intent.getStringExtra("arrivalPlace");
        departurePlace = intent.getStringExtra("departurePlace");
        shipId = intent.getStringExtra("shipId");
        token = intent.getStringExtra("tokenId");
        mIobserved = intent.getBooleanExtra("mIobserved", false);

        textViewShipName.setText(shipName);
        textViewShipDate.setText(shipDate);
        textViewShipTime.setText(shipTime);
        textViewShipInfo.setText(shipInfo);
        textViewShipArrival.setText(arrivalPlace);
        textViewShipDeparture.setText(departurePlace);


        ImageButton watchBtn = findViewById(R.id.watchbtn);
        ImageButton watchNOTbtn =findViewById(R.id.watchNOTbtn);
        if (imFrom.equals("favorite")|| mIobserved) {
            watchBtn.setVisibility(INVISIBLE);
            watchNOTbtn.setVisibility(VISIBLE);
        }
        //else watchBtn.setVisibility(VISIBLE);watchNOTbtn.setVisibility(INVISIBLE);

        watchBtn.setOnClickListener((v) -> {
            DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(portName).child(shipId);
            databaseReference.child("tokens").push().setValue(token);
            Toast.makeText(this, "Dodano do Obserowanych", Toast.LENGTH_SHORT).show();
            Log.d("Obseruje", portName);

            watchBtn.setVisibility(INVISIBLE);
            watchNOTbtn.setVisibility(VISIBLE);
        });


        watchNOTbtn.setOnClickListener((v->{

            final Map<String, Object> tokensMap = new HashMap<>();
            final DatabaseReference refernceToTokens = FirebaseDatabase.getInstance().getReference().child(portName).child(shipId).child("tokens");
            refernceToTokens.addListenerForSingleValueEvent(new ValueEventListener() {


                @Override

                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    for (DataSnapshot fauvoriteShipTokens : dataSnapshot.getChildren()) {
                        if (fauvoriteShipTokens.getValue().toString().equals(token))
                            tokensMap.put(fauvoriteShipTokens.getKey(), null);
                    }
                    refernceToTokens.updateChildren(tokensMap);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
            Toast.makeText(this, "Usunieto z Obserowanych", Toast.LENGTH_SHORT).show();
            watchBtn.setVisibility(VISIBLE);
            watchNOTbtn.setVisibility(INVISIBLE);


        }));


//        public void watchNOTbtn ( final View view){
//
//            final Map<String, Object> tokensMap = new HashMap<>();
//            final DatabaseReference refernceToTokens = FirebaseDatabase.getInstance().getReference().child(portName).child(shipId).child("tokens");
//            refernceToTokens.addListenerForSingleValueEvent(new ValueEventListener() {
//
//
//                @Override
//
//                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//
//                    for (DataSnapshot fauvoriteShipTokens : dataSnapshot.getChildren()) {
//                        if (fauvoriteShipTokens.getValue().toString().equals(token))
//                            tokensMap.put(fauvoriteShipTokens.getKey(), null);
//                    }
//                    refernceToTokens.updateChildren(tokensMap);
//                }
//
//                @Override
//                public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                }
//            });
//            Toast.makeText(this, "Usunieto z Obserowanych", Toast.LENGTH_SHORT).show();
//
//
//        }


//    public void watchBTN(View view) {
//        if (imFrom.equals("favorite")) {
//            view.setVisibility(View.INVISIBLE);
//        }
//
//        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(portName).child(shipId);
//        databaseReference.child("tokens").push().setValue(token);
//        Toast.makeText(this, "Dodano do Obserowanych", Toast.LENGTH_SHORT).show();
//        Log.d("Obseruje", portName);
//
}



    @Override
        protected void onPause () {
            super.onPause();
            Log.d("ACTIVITY", "OnPause");
            if (imFrom.equals("favorite")) {
                Intent passingBy = new Intent(DeilWindow.this, MainActivity.class);
                passingBy.putExtra("passingBy", imFrom);
                passingBy.putExtra("portZintenta", portName);
                DeilWindow.this.startActivity(passingBy);
            } else {
                Intent passingBy = new Intent(DeilWindow.this, MainActivity.class);
                passingBy.putExtra("passingBy", imFrom);
                passingBy.putExtra("portZintenta", portName);
                DeilWindow.this.startActivity(passingBy);
            }
        }

        @Override
        protected void onStop () {
            super.onStop();
            Log.d("ACTIVITY", "OnStop");





        Log.d("OnCreate", "OnCreate");
    }



    }


